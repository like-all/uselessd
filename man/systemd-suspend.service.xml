<?xml version='1.0'?> <!--*-nxml-*-->
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
        "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">

<!--
  This file is part of systemd.

  Copyright 2012 Lennart Poettering
  Copyright 2013 Zbigniew Jędrzejewski-Szmek

  systemd is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation; either version 2.1 of the License, or
  (at your option) any later version.

  systemd is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with systemd; If not, see <http://www.gnu.org/licenses/>.
-->

<refentry id="systemd-suspend.service">

        <refentryinfo>
                <title>uselessd-suspend.service</title>
                <productname>uselessd</productname>

                <authorgroup>
                        <author>
                                <contrib>Developer</contrib>
                                <firstname>The Initfinder</firstname>
                                <surname>General</surname>
                                <email>wat@meworry.com</email>
                        </author>
                </authorgroup>
        </refentryinfo>

        <refmeta>
                <refentrytitle>uselessd-suspend.service</refentrytitle>
                <manvolnum>8</manvolnum>
        </refmeta>

        <refnamediv>
                <refname>uselessd-suspend.service</refname>
                <refname>uselessd-hibernate.service</refname>
                <refname>uselessd-hybrid-sleep.service</refname>
                <refname>uselessd-sleep</refname>
                <refpurpose>System sleep state logic</refpurpose>
        </refnamediv>

        <refsynopsisdiv>
                <para><filename>uselessd-suspend.service</filename></para>
                <para><filename>uselessd-hibernate.service</filename></para>
                <para><filename>uselessd-hybrid-sleep.service</filename></para>
                <para><filename>/usr/lib/systemd/system-sleep</filename></para>
        </refsynopsisdiv>

        <refsect1>
                <title>Description</title>

                <para><filename>uselessd-suspend.service</filename> is
                a system service that is pulled in by
                <filename>suspend.target</filename> and is responsible
                for the actual system suspend. Similarly,
                <filename>uselessd-hibernate.service</filename> is
                pulled in by <filename>hibernate.target</filename> to
                execute the actual hibernation. Finally,
                <filename>uselessd-hybrid-sleep.service</filename> is
                pulled in by <filename>hybrid-sleep.target</filename>
                to execute hybrid hibernation with system
                suspend.</para>

                <para>Immediately before entering system suspend
                and/or hibernation
                <filename>uselessd-suspend.service</filename> (and the
                other mentioned units, respectively) will run all
                shell scripts with the .sh extension in
                <filename>/usr/lib/systemd/system-sleep/.</filename>
                </para>

                <para>Note that scripts dropped in
                <filename>/usr/lib/systemd/system-sleep/</filename>
                are intended for local use only and should be
                considered hacks. If applications want to be notified
                of system suspend/hibernation and resume, there are
                much nicer interfaces available.</para>

                <para>Note that
                <filename>uselessd-suspend.service</filename>,
                <filename>uselessd-hibernate.service</filename>, and
                <filename>uselessd-hybrid-sleep.service</filename>
                should never be executed directly. Instead, trigger
                system sleep states with a command such as
                <literal>systemctl suspend</literal> or
                similar.</para>

                <para>Internally, this service will echo a string like
                <literal>mem</literal> into
                <filename>/sys/power/state</filename>, to trigger the
                actual system suspend. What exactly is written
                where can be configured in the <literal>[Sleep]</literal>
                section of <filename>/etc/systemd/sleep.conf</filename>.
                See <citerefentry><refentrytitle>systemd-sleep.conf</refentrytitle><manvolnum>5</manvolnum></citerefentry>.
                </para>
        </refsect1>

        <refsect1>
                <title>Options</title>

                <para><command>uselessd-sleep</command> understands the
                following commands:</para>

                <variablelist>
                        <varlistentry>
                                <term><option>-h</option></term>
                                <term><option>--help</option></term>

                                <listitem><para>Print a short help
                                text and exit.</para></listitem>
                        </varlistentry>
                        <varlistentry>
                                <term><option>--version</option></term>

                                <listitem><para>Print the systemd version
                                identifier and exit.</para></listitem>
                        </varlistentry>
                        <varlistentry>
                                <term><option>suspend</option></term>
                                <term><option>hibernate</option></term>
                                <term><option>hybrid-sleep</option></term>

                                <listitem><para>Suspend, hibernate, or
                                put the system to hybrid sleep.</para>
                                </listitem>
                        </varlistentry>
                </variablelist>
        </refsect1>

        <refsect1>
                <title>See Also</title>
                <para>
                        <citerefentry><refentrytitle>systemd-sleep.conf</refentrytitle><manvolnum>5</manvolnum></citerefentry>,
                        <citerefentry><refentrytitle>systemd</refentrytitle><manvolnum>1</manvolnum></citerefentry>,
                        <citerefentry><refentrytitle>systemctl</refentrytitle><manvolnum>1</manvolnum></citerefentry>,
                        <citerefentry><refentrytitle>systemd.special</refentrytitle><manvolnum>7</manvolnum></citerefentry>,
                        <citerefentry><refentrytitle>systemd-halt.service</refentrytitle><manvolnum>8</manvolnum></citerefentry>
                </para>
        </refsect1>

</refentry>
